package main;

import palabras.Libreria;
import palabras.Palabra;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.Scanner;

public class Ahorcado {

    public static void main(String[] args) {

        Scanner tec = new Scanner(System.in);
        ArrayList<Palabra> palabras = new ArrayList<>();
        File fichero = new File("palabras.txt");

        boolean continuar = true;
        int opcion = menu();
        // Cargar las palabras:
        try {
            palabras = Libreria.cargarPalabras(fichero, palabras);
            Collections.sort(palabras);
            System.out.println("Hay " + palabras.size() + " palabras cargadas.");

        } catch (IOException e) {
            System.out.println("No se pudo crear el fichero");
            e.printStackTrace();
        }

        while (continuar) {

            if (opcion == 2) { // Añadir Palabra
                //System.out.println("Hay " + palabras.size() + " palabras cargadas.");
                System.out.println("Escriba las palabras que desee añadir a la lista, escriba \"-\" cuando haya acabado:");
                boolean seguirAnadiendo = true;

                while (seguirAnadiendo) {
                    String nuevaPalabra = tec.next();
                    try {
                        if (nuevaPalabra.equals("-")) {
                            seguirAnadiendo = false;
                        } else {
                            Libreria.anadirPalabra(fichero, nuevaPalabra, palabras);
                        }
                    } catch (FileNotFoundException e) {
                        System.out.println("No se pudo encontrar el fichero.");
                        e.printStackTrace();
                    } catch (IOException e) {
                        System.out.println("Error");
                        e.printStackTrace();
                    }
                }
            } else if (opcion == 4) { // Salir
                continuar = false;
            } else if (opcion == 3) { // Listar Palabras
                System.out.println("Lista de Palabras:");
                System.out.println();

                for (Palabra s : palabras) {
                    // Imprimir con la primera letra en mayúscula:
                    System.out.println(s.getNombre().substring(0, 1).toUpperCase() + s.getNombre().substring(1).toLowerCase());
                }
                System.out.println();
                System.out.println("Pulse ENTER para continuar.");
                tec.nextLine();
            } else if (opcion == 1) { // JUGAR
                Random r = new Random();
                int palabraRandom = r.nextInt(palabras.size());
                System.out.println(palabraRandom);
            }

            if (continuar) { // Esto evita que el menú salga dos veces. Es muy cutre pero funciona, así que gñe.
                opcion = menu();
            }
        }

        System.out.println("Gracias por jugar.");
    }

    public static int menu() {

        Scanner tec = new Scanner(System.in);

        System.out.println("====================================================================");
        System.out.println("Bienvenido a \"Ahorcado\". Un juego creado en Java por Alberto Díaz.");
        System.out.println("====================================================================");
        System.out.println("Elija una opción:");
        System.out.println("====================================================================");
        System.out.println("1. Empezar a Jugar");
        System.out.println("2. Añadir una palabra");
        System.out.println("3. Listar Palabras.");
        System.out.println("4. Salir.");
        System.out.println("====================================================================");

        int opcion = tec.nextInt();

        return opcion;
    }
}
